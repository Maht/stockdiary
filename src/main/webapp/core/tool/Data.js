
plat.Data= function (){
	/**
	 * 存储数据
	 */
	var _datas={};
	
	function getValue(key) {
		if(window.top!=window){
			return window.top.plat.Data.getValue(key);
		}
		
		if(!key){
			return null;
		}
		var re=_datas[key];
		if((typeof re)=="undefined"){   //不存在同步加载值集
			synchLoad(key);
			re=_datas[key];
		}
		return re;
	};
	
	/**
	 * 
	 */
	function loadData(prefCodes,params,callback){
		if(window.top!=window){
			return window.top.plat.Data.loadData(prefCodes,params,callback);
		}
		
		if(!prefCodes){
			return;
		}
		if($.type(prefCodes)==="object"){
			$.extend(_datas,prefCodes);
			return;
		}
		
		if(!params){
			params={};
		}
		params.prefCodes=prefCodes;
		$.post(plat.fullUrl("/core/preference/get.do"), params,
			function(data) {
				if((typeof callback)=='function'){
					callback();
				}
				$.extend(_datas,data);
			});
	};
	
	/**
	 * 同步加载
	 */
	function synchLoad(prefCode,params){
		if(window.top!=window){
			return window.top.plat.Data.synchLoad(prefCode,params);
		}
		if(!prefCode){
			return;
		}
		if(!params){
			params={};
		}
		params.prefCodes=prefCode;
		var a=$.ajax({
			   type: "POST",
			   url: plat.fullUrl("/core/preference/get.do"),
			   data: params,
			   dataType:"json",
			   async : false
			});
		data=eval("("+a.responseText+")");
		$.extend(_datas,data);
		return data.prefCode;
	};
	
	//保存到服务器
	function saveData(data,callback){
		loadData(data);
		for(var one in data){
			$.post(plat.fullUrl("/core/preference/saveValue.do"),
				{prefCode:one,prefValue:data[one]},
				callback
			);
		}
	}
	
	return{
		getServerURL : function() {
			return plat.Data.getValue('serverURL');
		},
		getContextPath : function() {
			return plat.Data.getValue('contextPath');
		},
		loadData : loadData,
		synchLoad :synchLoad,
		getValue : getValue,
		saveData: saveData
	};
}();