<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>

</head>

<body>
	<div id="toolbar">
       <a href="#" class="easyui-linkbutton" onclick="click_save()" iconCls='icon-save'>保存</a>
       <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dg').edatagrid('cancelRow')">取消</a>
	</div>
	<table id="dg" class="easyui-edatagrid" toolbar="#toolbar"
		data-options="rownumbers:true,singleSelect:true,pagination:false,method:'post',idField:'feeId',
				remoteSort:false,multiSort:true,sortName:'stockMarket,tradeCode',sortOrder:'asc,asc',
				fit:true,
				autoSave:true,
				url : plat.fullUrl('/stock/tradefee/query.do'),
				saveUrl:plat.fullUrl('/stock/tradefee/save.do')" 
			>
        <thead>
            <tr>
            	<th data-options="field:'feeId',width:10,hidden:true,rowspan:2">主键</th>
            	<th data-options="field:'stockMarket',width:80,sortable:true,rowspan:2,flexset:'SD_MARKET'">证券市场</th>
                <th data-options="field:'tradeCode',width:80,sortable:true,rowspan:2,flexset:'SD_STOCK_TRADE'">交易类型</th>
				<th data-options="field:'feeCommision',colspan:4,align:'center'">佣金</th>
                <th data-options="field:'feeTax',colspan:4,align:'center'">印花税</th>           
                <th data-options="field:'feeOther',colspan:4,align:'center'">过户费</th>
            </tr>
            <tr>
				<th data-options="field:'feeCommisionBase',width:80,flexset:'SD_FEE_BASE',editor:'combobox'">基础</th>
                <th data-options="field:'feeCommisionRate',width:80,editor:{type:'numberbox',options:{precision:2}}">费率(%)</th>
                <th data-options="field:'feeCommisionMin',width:60,editor:{type:'numberbox',options:{precision:2}}">最小</th>
                <th data-options="field:'feeCommisionMax',width:60,editor:{type:'numberbox',options:{precision:2}}">最大</th>
                <th data-options="field:'feeTaxBase',width:80,flexset:'SD_FEE_BASE',editor:'combobox'">基础</th>
                <th data-options="field:'feeTaxRate',width:80,editor:{type:'numberbox',options:{precision:2}}">费率(%)</th>
                <th data-options="field:'feeTaxMin',width:60,editor:{type:'numberbox',options:{precision:2}}">最小</th>
                <th data-options="field:'feeTaxMax',width:60,editor:{type:'numberbox',options:{precision:2}}">最大</th>               
                <th data-options="field:'feeOtherBase',width:80,flexset:'SD_FEE_BASE',editor:'combobox'">基础</th>
                <th data-options="field:'feeOtherRate',width:80,editor:{type:'numberbox',options:{precision:2}}">费率(%)</th>
                <th data-options="field:'feeOtherMin',width:60,editor:{type:'numberbox',options:{precision:2}}">最小</th>
                <th data-options="field:'feeOtherMax',width:60,editor:{type:'numberbox',options:{precision:2}}">最大</th>
            </tr>
        </thead>
    </table>
</body>
</html>
