package com.cch.platform.core.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * CorePrefData entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "core_pref_data")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CorePrefData implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -5918463064181334088L;
	private String prefCode;
	private String prefOwner;
	private String prefValue;

	// Constructors

	/** default constructor */
	public CorePrefData() {
	}

	/** min constructor */
	public CorePrefData(String prefCode, String prefOwner) {
		this.prefCode = prefCode;
		this.prefOwner = prefOwner;
	}
	
	/** full constructor */
	public CorePrefData(String prefCode, String prefOwner, String prefValue) {
		this.prefCode = prefCode;
		this.prefOwner = prefOwner;
		this.prefValue = prefValue;
	}

	@Id
	@Column(name = "pref_code", nullable = false, length = 100)
	public String getPrefCode() {
		return this.prefCode;
	}

	public void setPrefCode(String prefCode) {
		this.prefCode = prefCode;
	}
	@Id
	@Column(name = "pref_owner", nullable = false, length = 100)
	public String getPrefOwner() {
		return this.prefOwner;
	}

	public void setPrefOwner(String prefOwner) {
		this.prefOwner = prefOwner;
	}

	@Column(name = "pref_value", length = 500)
	public String getPrefValue() {
		return this.prefValue;
	}

	public void setPrefValue(String prefValue) {
		this.prefValue = prefValue;
	}
	
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CorePrefData))
			return false;
		CorePrefData castOther = (CorePrefData) other;

		return ((this.getPrefCode() == castOther.getPrefCode()) || (this
				.getPrefCode() != null && castOther.getPrefCode() != null && this
				.getPrefCode().equals(castOther.getPrefCode())))
				&& ((this.getPrefOwner() == castOther.getPrefOwner()) || (this
						.getPrefOwner() != null
						&& castOther.getPrefOwner() != null && this
						.getPrefOwner().equals(castOther.getPrefOwner())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getPrefCode() == null ? 0 : this.getPrefCode().hashCode());
		result = 37 * result
				+ (getPrefOwner() == null ? 0 : this.getPrefOwner().hashCode());
		return result;
	}

}