package com.cch.stock.set.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cch.platform.web.WebUtil;
import com.cch.stock.set.bean.SdTradeFee;
import com.cch.stock.set.service.TradeFeeService;

@Controller
@RequestMapping(value = "/stock/tradefee")
public class TradeFee {

	@Autowired
	private TradeFeeService service;
	
	@RequestMapping(value = "/query.do")
	public ModelAndView  query(ServletRequest request) throws Exception {
		Map<String,Object> re=new HashMap<String,Object>();
		re.put("rows", service.getAll());
		return new ModelAndView("jsonView",re);
	}

	@RequestMapping(value = "/save.do")
	public String save(SdTradeFee fee) {
		service.saveOrUpdate(fee);
		return ("jsonView");
	}
	
	@RequestMapping(value = "/getFee.do")
	public String getFee(ServletRequest request, ModelMap mm) {
		Map<String,Object> param=WebUtil.getParam(request);
		if(param.get("billCatory")!=null&&param.get("stockCode")!=null){
			Map<String,Object> re=service.getFee(param);
			mm.addAllAttributes(re);
		}
		return ("jsonView");
	}
}
